# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

USB_HAL_PATH =
    rebase_path("//vendor/${product_company}/${product_name}/hals/usb")
cmd = "if [ -f $USB_HAL_PATH/product.gni ]; then echo true; else echo false; fi"
HAVE_USB_HAL_PATH =
    exec_script("//build/lite/run_shell_cmd.py", [ cmd ], "value")
if (HAVE_USB_HAL_PATH) {
  import("$USB_HAL_PATH/product.gni")
} else {
  enable_linux_native_mode = false
}

group("libusb_core") {
  deps = [
    ":libusb_ddk_device",
    ":libusb_ddk_host",
    ":libusb_pnp_manager",
  ]
}

ohos_shared_library("libusb_ddk_host") {
  include_dirs = [
    "device/include",
    "host/include",
    "//drivers/peripheral/usb/interfaces/ddk/common",
    "//drivers/peripheral/usb/interfaces/ddk/device",
    "//drivers/peripheral/usb/interfaces/ddk/host",
  ]
  sources = [
    "host/src/linux_adapter.c",
    "host/src/usb_interface_pool.c",
    "host/src/usb_io_manage.c",
    "host/src/usb_protocol.c",
    "host/src/usb_raw_api.c",
    "host/src/usb_raw_api_library.c",
  ]

  if (enable_linux_native_mode) {
    defines = [ "USB_EVENT_NOTIFY_LINUX_NATIVE_MODE" ]
  }

  if (is_standard_system) {
    external_deps = [
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_utils",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_usb"
}

ohos_shared_library("libusb_pnp_manager") {
  include_dirs = [
    "$hdf_framework_path/model/usb/include",
    "host/include",
  ]
  sources = [
    "$hdf_framework_path/model/usb/src/usb_ddk_pnp_loader.c",
    "host/src/ddk_device_manager.c",
    "host/src/ddk_pnp_listener_mgr.c",
    "host/src/ddk_sysfs_device.c",
    "host/src/ddk_uevent_handle.c",
    "host/src/usb_pnp_manager.c",
  ]

  if (enable_linux_native_mode) {
    defines = [ "USB_EVENT_NOTIFY_LINUX_NATIVE_MODE" ]
  }

  if (is_standard_system) {
    external_deps = [
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_usb"
}

ohos_shared_library("libusb_ddk_device") {
  include_dirs = [
    "device/include",
    "host/include",
    "//drivers/peripheral/usb/interfaces/ddk/common",
    "//drivers/peripheral/usb/interfaces/ddk/device",
    "//drivers/peripheral/usb/interfaces/ddk/host",
  ]
  sources = [
    "device/src/adapter_if.c",
    "device/src/usbfn_cfg_mgr.c",
    "device/src/usbfn_dev_mgr.c",
    "device/src/usbfn_io_mgr.c",
    "device/src/usbfn_sdk_if.c",
  ]

  if (is_standard_system) {
    external_deps = [
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_utils",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  if (enable_linux_native_mode) {
    defines = [ "USB_EVENT_NOTIFY_LINUX_NATIVE_MODE" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_usb"
}
