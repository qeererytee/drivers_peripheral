#include "ril_interface_impl.h"
extern "C" {
#include "hril_hdf.h"
}
#include <hdf_base.h>
#include <hdf_log.h>

#include "hril_request.h"
namespace OHOS {
namespace HDI {
namespace Ril {
namespace V1_0 {
static std::mutex mutex_;
static sptr<IRilCallback> callback_;
namespace {
sptr<RilInterfaceImpl::RilDeathRecipient> g_deathRecipient = nullptr;
}
extern "C" IRilInterface *RilInterfaceImplGetInstance(void)
{
    HDF_LOGE("RilInterfaceImplGetInstance start");
    HDF_LOGE("%{public}s: HdfRilInterfaceDriverBind start", __func__);
    using OHOS::HDI::Ril::V1_0::RilInterfaceImpl;
    RilInterfaceImpl *service = new (std::nothrow) RilInterfaceImpl();
    if (service == nullptr) {
        return nullptr;
    }
    HDF_LOGI("RilInterfaceImplGetInstance LoadVendor() start");
    service->SetHRilOps(LoadVendor());
    HDF_LOGI("RilInterfaceImplGetInstance service->Init start");
    if (service->Init() != HDF_SUCCESS) {
        delete service;
        return nullptr;
    }
    HDF_LOGI("RilInterfaceImplGetInstance end");
    return service;
}

int32_t RilInterfaceImpl::SetEmergencyCallList(int32_t slotId, const IEmergencyInfoList &emergencyInfoList)
{
    return TaskSchedule("CallProvider", callProvider_, &CallProvider::SetEmergencyCallList, slotId, emergencyInfoList);
}

int32_t RilInterfaceImpl::GetEmergencyCallList(int32_t slotId, int32_t serialId)
{
    return TaskSchedule("CallProvider", callProvider_, &CallProvider::GetEmergencyCallList, slotId, serialId);
}

int32_t RilInterfaceImpl::SetCallback(const sptr<IRilCallback> &rilCallback)
{
    std::lock_guard<std::mutex> lock(mutex_);
    callback_ = rilCallback;
    if (callback_ == nullptr) {
        HDF_LOGE("SetCallback fail callback_ is null");
        UnRegister();
        return HDF_SUCCESS;
    }
    g_deathRecipient = new RilDeathRecipient(this);
    if (g_deathRecipient == nullptr) {
        HDF_LOGE("SetCallback fail g_deathRecipient is null");
        return HDF_FAILURE;
    }
    AddRilDeathRecipient(callback_);
    Telephony::HRilManager::manager_->SetRilCallback(callback_);
    return HDF_SUCCESS;
}

int32_t RilInterfaceImpl::AddRilDeathRecipient(const sptr<IRilCallback> &callback)
{
    HDF_LOGI("AddRilDeathRecipient");
    const sptr<IRemoteObject> &remote = OHOS::HDI::hdi_objcast<IRilCallback>(callback);
    bool result = remote->AddDeathRecipient(g_deathRecipient);
    if (!result) {
        HDF_LOGI("AddRilDeathRecipient fail");
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

int32_t RilInterfaceImpl::RemoveRilDeathRecipient(const sptr<IRilCallback> &callback)
{
    HDF_LOGI("RemoveRilDeathRechansipient");
    const sptr<IRemoteObject> &remote = OHOS::HDI::hdi_objcast<IRilCallback>(callback);
    bool result = remote->RemoveDeathRecipient(g_deathRecipient);
    if (!result) {
        HDF_LOGI("RemoveRilDeathRecipient fail");
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

void RilInterfaceImpl::RilDeathRecipient::OnRemoteDied(const wptr<IRemoteObject> &object)
{
    rilInterfaceImpl_->UnRegister();
}

int32_t RilInterfaceImpl::UnRegister()
{
    HDF_LOGI("UnRegister");
    RemoveRilDeathRecipient(callback_);
    callback_ = nullptr;
    Telephony::HRilManager::manager_->SetRilCallback(nullptr);
    return HDF_SUCCESS;
}

int32_t RilInterfaceImpl::Init()
{
    HDF_LOGI("RilInterfaceImpl::Init start");
    callProvider_.RegisterFuncs(hrilOps_.callOps);
    HDF_LOGI("RilInterfaceImpl::Init end");
    return HDF_SUCCESS;
}
} // namespace V1_0
} // namespace Ril
} // namespace HDI
} // namespace OHOS
