#include "call_provider.h"

#include <hdf_base.h>
#include <hdf_log.h>

#include "hril_hdf.h"
#include "hril_manager.h"
#include "hril_request.h"

namespace OHOS {
namespace HDI {
namespace Ril {
namespace V1_0 {
int32_t CallProvider::SetEmergencyCallList(int32_t slotId, const IEmergencyInfoList &emergencyInfoList)
{
    int size = (int)emergencyInfoList.calls.size();
    if (size <= 0) {
        HDF_LOGE("SetEmergencyCallList RilAdapter failed to do ReadFromParcel! calls len 0");
        return HDF_ERR_INVALID_PARAM;
    }
    HRilEmergencyInfo emergencyInfoCalls[size];
    int32_t serial = emergencyInfoList.flag;
    CopyToHRilEmergencyInfoArray(emergencyInfoCalls, emergencyInfoList.calls);
    if (Telephony::HRilManager::manager_ == nullptr) {
        HDF_LOGE("SetEmergencyCallList manager_ is null");
        return HDF_FAILURE;
    }
    ReqDataInfo *requestInfo =
        Telephony::HRilManager::manager_->CreateHRilRequest(serial, slotId, HREQ_CALL_SET_EMERGENCY_LIST);
    if (requestInfo == nullptr) {
        HDF_LOGE("SetEmergencyCallList RilAdapter failed to do Create HRilRequest!");
        return HDF_FAILURE;
    }

    if (callFuncs_ == nullptr) {
        HDF_LOGE("SetEmergencyCallList dataFuncs_ is null");
        return HDF_FAILURE;
    }

    callFuncs_->SetEmergencyCallList(requestInfo, emergencyInfoCalls, size);
    return HDF_SUCCESS;
}

int32_t CallProvider::GetEmergencyCallList(int32_t slotId, int32_t serialId)
{
    if (Telephony::HRilManager::manager_ == nullptr) {
        HDF_LOGE("GetEmergencyCallList manager_ is null");
        return HDF_FAILURE;
    }
    ReqDataInfo *requestInfo =
        Telephony::HRilManager::manager_->CreateHRilRequest(serialId, slotId, HREQ_CALL_GET_EMERGENCY_LIST);
    if (requestInfo == nullptr) {
        HDF_LOGE("GetEmergencyCallList RilAdapter failed to do Create HRilRequest!");
        return HDF_FAILURE;
    }

    if (callFuncs_ == nullptr) {
        HDF_LOGE("GetEmergencyCallList dataFuncs_ is null");
        return HDF_FAILURE;
    }
    callFuncs_->GetEmergencyCallList(requestInfo);
    return HDF_SUCCESS;
}

void CallProvider::CopyToHRilEmergencyInfoArray(
    HRilEmergencyInfo *emergencyInfoCalls, std::vector<IEmergencyCall> calls)
{
    for (unsigned int i = 0; i < calls.size(); i++) {
        auto call = calls.at(i);
        emergencyInfoCalls[i].index = call.index;
        emergencyInfoCalls[i].total = call.total;
        char *eccNum = new char[call.eccNum.size() + 1];
        if (strcpy_s(eccNum, call.eccNum.size() + 1, call.eccNum.c_str()) == EOK) {
            emergencyInfoCalls[i].eccNum = eccNum;
        } else {
            delete[] eccNum;
        }
        emergencyInfoCalls[i].category = static_cast<int32_t>(call.eccType);
        emergencyInfoCalls[i].simpresent = call.simpresent;
        char *mcc = new char[call.mcc.size() + 1];
        if (strcpy_s(mcc, call.mcc.size() + 1, call.mcc.c_str()) == EOK) {
            emergencyInfoCalls[i].mcc = mcc;
        } else {
            delete[] mcc;
        }
        emergencyInfoCalls[i].abnormalService = call.abnormalService;
    }
}

void CallProvider::RegisterFuncs(const HRilCallReq *callFuncs)
{
    callFuncs_ = callFuncs;
}
} // namespace V1_0
} // namespace Ril
} // namespace HDI
} // namespace OHOS
