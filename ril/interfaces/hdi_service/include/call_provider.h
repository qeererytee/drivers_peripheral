/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_HDI_RIL_V1_0_CALL_PROVIDER_H
#define OHOS_HDI_RIL_V1_0_CALL_PROVIDER_H
#include "hril_manager.h"
#include "v1_0/iril_interface.h"
#include "vector"

namespace OHOS {
namespace HDI {
namespace Ril {
namespace V1_0 {
class CallProvider {
public:
    CallProvider() = default;
    virtual ~CallProvider() = default;
    int32_t SetEmergencyCallList(int32_t slotId, const IEmergencyInfoList &emergencyInfoList);
    int32_t GetEmergencyCallList(int32_t slotId, int32_t serialId);
    void RegisterFuncs(const HRilCallReq *dataFuncs);

private:
    const HRilCallReq *callFuncs_ = nullptr;
    void CopyToHRilEmergencyInfoArray(HRilEmergencyInfo *emergencyInfoCalls, std::vector<IEmergencyCall> calls);
};
} // namespace V1_0
} // namespace Ril
} // namespace HDI
} // namespace OHOS

#endif // OHOS_HDI_RIL_V1_0_RILIMPL_H
