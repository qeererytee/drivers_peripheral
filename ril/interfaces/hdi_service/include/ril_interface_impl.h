/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_HDI_RIL_V1_0_RIL_INTERFACE_IMPL_H
#define OHOS_HDI_RIL_V1_0_RIL_INTERFACE_IMPL_H
#include <hdf_log.h>
#include <iproxy_broker.h>
#include <iremote_object.h>
#include <pthread.h>

#include "call_provider.h"
#include "hril_manager.h"
#include "hril_types.h"
#include "v1_0/iril_interface.h"
#include "vector"

namespace OHOS {
namespace HDI {
namespace Ril {
namespace V1_0 {
class RilInterfaceImpl : public IRilInterface {
public:
    RilInterfaceImpl() = default;
    virtual ~RilInterfaceImpl() = default;
    int32_t SetEmergencyCallList(int32_t slotId, const IEmergencyInfoList &emergencyInfoList) override;
    int32_t GetEmergencyCallList(int32_t slotId, int32_t serialId) override;
    int32_t SetCallback(const sptr<IRilCallback> &rilCallback) override;
    int32_t Init();
    void SetHRilOps(const HRilOps *hrilOps)
    {
        if (hrilOps == nullptr) {
            return;
        }
        (void)memcpy_s(&hrilOps_, sizeof(HRilOps), hrilOps, sizeof(HRilOps));
    }
    class RilDeathRecipient : public IRemoteObject::DeathRecipient {
    public:
        explicit RilDeathRecipient(const wptr<RilInterfaceImpl> &rilInterfaceImpl)
            : rilInterfaceImpl_(rilInterfaceImpl) {};
        virtual ~RilDeathRecipient() = default;
        virtual void OnRemoteDied(const wptr<IRemoteObject> &object) override;

    private:
        wptr<RilInterfaceImpl> rilInterfaceImpl_;
    };
    template<typename ClassTypePtr, typename FuncType, typename... ParamTypes>
    inline int32_t TaskSchedule(
        const std::string _module, ClassTypePtr &_obj, FuncType &&_func, ParamTypes &&... _args) const
    {
        if (_func != nullptr) {
            // The reason for using native member function access here is to
            //   remove std::unique_ptr to prevent copying.
            // The reason for not directly using pointers to access member functions is:
            //   _obj is a smart pointer, not a native pointer.
            static pthread_mutex_t dispatchMutex_ = PTHREAD_MUTEX_INITIALIZER;
            pthread_mutex_lock(&dispatchMutex_);
            auto ret = (_obj.*(_func))(std::forward<ParamTypes>(_args)...);
            pthread_mutex_unlock(&dispatchMutex_);
            return ret;
        } else {
            HDF_LOGE("%{public}s - this %{public}p: %{public}s", _module.c_str(), &_obj, "null pointer");
            return HRIL_ERR_NULL_POINT;
        }
    }

private:
    HRilOps hrilOps_ = { 0 };
    CallProvider callProvider_;
    int32_t UnRegister();
    int32_t AddRilDeathRecipient(const sptr<IRilCallback> &callback);
    int32_t RemoveRilDeathRecipient(const sptr<IRilCallback> &callback);
}; // namespace V1_0
} // namespace V1_0
} // namespace Ril
} // namespace HDI
} // namespace OHOS

#endif // OHOS_HDI_RIL_V1_0_RILIMPL_H
