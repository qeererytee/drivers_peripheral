# Copyright (c) 2022 Shenzhen Kaihong DID Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

ohos_shared_library("libcodec_hdi_omx_server") {
  include_dirs = [
    "//drivers/peripheral/codec/interfaces/include",
    "//drivers/peripheral/codec/hal/v2.0/hdi_impl/include",
    "//drivers/peripheral/codec/hal/include",
    "//third_party/openmax/api/1.1.2",
    "//drivers/peripheral/base",
  ]
  sources = [
    "src/codec_adapter.cpp",
    "src/codec_callback_type_proxy.c",
    "src/codec_component_capability_config.c",
    "src/codec_component_manager_service.c",
    "src/codec_component_manager_stub.c",
    "src/codec_component_type_driver.c",
    "src/codec_component_type_service.c",
    "src/codec_component_type_stub.c",
    "src/codec_config_parser.c",
    "src/codec_types.c",
    "v2.0/hdi_impl/src/codec_dyna_buffer.cpp",
    "v2.0/hdi_impl/src/codec_handle_buffer.cpp",
    "v2.0/hdi_impl/src/codec_omx_core.cpp",
    "v2.0/hdi_impl/src/codec_share_buffer.cpp",
    "v2.0/hdi_impl/src/component_mgr.cpp",
    "v2.0/hdi_impl/src/component_node.cpp",
    "v2.0/hdi_impl/src/icodec_buffer.cpp",
  ]

  cflags_cc = [
    "-Wall",
    "-Wextra",
    "-Werror",
    "-Wno-#warnings",
    "-Wno-missing-braces",
    "-Wno-missing-field-initializers",
  ]

  if (is_standard_system) {
    deps = [ "//third_party/libdrm:libdrm" ]
    external_deps = [
      "graphic_chipsetsdk:buffer_handle",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

ohos_shared_library("libcodec_hdi_omx_client") {
  include_dirs = [
    "//drivers/hdf_core/adapter/uhdf2/include/hdi",
    "//drivers/peripheral/codec/interfaces/include",
    "//drivers/peripheral/codec/hal/include",
    "//third_party/openmax/api/1.1.2",
    "//drivers/peripheral/base",
  ]
  sources = [
    "src/codec_callback_type_stub.c",
    "src/codec_component_manager_proxy.c",
    "src/codec_component_type_proxy.c",
    "src/codec_types.c",
  ]

  if (is_standard_system) {
    deps = [ "//third_party/libdrm:libdrm" ]

    external_deps = [
      "graphic_chipsetsdk:buffer_handle",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

ohos_shared_library("libcodec_hdi_omx_callback_type_service_impl") {
  include_dirs = [
    "//drivers/hdf_core/adapter/uhdf2/include/hdi",
    "//drivers/peripheral/codec/interfaces/include",
    "//drivers/peripheral/codec/hal/include",
    "//third_party/openmax/api/1.1.2",
  ]
  sources = [ "src/codec_callback_type_service.c" ]

  if (is_standard_system) {
    external_deps = [
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_codec"
}

group("codec_hdi_omx") {
  deps = [
    ":libcodec_hdi_omx_callback_type_service_impl",
    ":libcodec_hdi_omx_client",
    ":libcodec_hdi_omx_server",
  ]
}
